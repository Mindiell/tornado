# encoding: utf-8

import click
from datetime import datetime, timedelta
import json
import os
import shutil
import time

from bs4 import BeautifulSoup
from flask.cli import with_appcontext
import requests

import config


PATH_STRUCTURES = os.path.join(config.DATA_PATH, "structures")
PATH_STRUCTURE = os.path.join(config.DATA_PATH, "structures", "%s")
PATH_SEANCES = os.path.join(config.DATA_PATH, "seances", "%s")
PATH_SEANCE = os.path.join(PATH_SEANCES, "%s")
LIST_ACTIVITES = os.path.join(config.DATA_PATH, "activites")
PATH_ACTIVITES = os.path.join(LIST_ACTIVITES, "%s")
PATH_ACTIVITE = os.path.join(PATH_ACTIVITES, "%s")
PATH_INSCRIPTIONS = os.path.join(config.DATA_PATH, "inscriptions", "%s")
PATH_INSCRIPTION = os.path.join(PATH_INSCRIPTIONS, "%s")
PATH_UTILISATEURS = os.path.join(config.DATA_PATH, "utilisateurs", "%s")


# ~ # Le serveur PEGASS est à la rue :/
# ~ requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += "HIGH:!DH:!aNULL"
# ~ try:
    # ~ requests.packages.urllib3.contrib.pyopenssl.DEFAULT_SSL_CIPHER_LIST += "HIGH:!DH:!aNULL"
# ~ except AttributeError:
    # ~ # no pyopenssl support used / needed / available
    # ~ pass

# Décorateur permettant de lisser les requêtes faites à PEGASS
def cool_request(function):
    def new_function(*args):
        if time.perf_counter() - args[0].last_request < 2:
            time.sleep(.5)
        args[0].last_request = time.perf_counter()
        return function(*args)
    return new_function


# Commandes spécifiques à PEGASS
class Pegass():
    def __init__(self):
        # Session à utiliser pour toutes les requêtes à venir
        self.session = requests.Session()
        self.session.headers.update({
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1"
        })
        self.logged = False
        self.last_request = time.perf_counter() - 1

    @cool_request
    def login(self):
        if self.logged:
            return True
        # Tentative de connexion à PEGASS
        self.session.get("https://pegass.croix-rouge.fr")
        result = self.session.post(
            "https://id.authentification.croix-rouge.fr/my.policy",
            data=config.CREDENTIALS
        )
        if "errorcode" in result.url:
            click.echo(" Erreur de connexion")
            return False
        # La double authentification nécessite de valider le formulaire reçu.
        soup = BeautifulSoup(result.content.decode("utf-8"), "lxml")
        url = soup.form.get("action")
        data = {}
        for form_input in soup.form.findAll("input"):
            if form_input.get("name") is not None:
                data[form_input.get("name")] = form_input.get("value")
        result = self.session.post(url, data=data)
        if "errorcode" in result.url:
            click.echo(" Erreur de connexion: %s" % result)
            return False
        self.logged = True
        return True

    @cool_request
    def get_structures(self, departement):
        # Recherche des structures en utilisant le département fourni
        url = "https://pegass.croix-rouge.fr/crf/rest/zonegeo/departement/%s" % departement
        result = self.session.get(url)
        if not result.ok:
            click.echo(" Erreur: %s" % result)
            return []
        return json.loads(result.content.decode("utf-8"))

    @cool_request
    def get_structure(self, structure_id):
        # Chargement d'unr structure spécifique
        url = "https://pegass.croix-rouge.fr/crf/rest/structure/%s" % structure_id
        result = self.session.get(url)
        if not result.ok:
            click.echo(" Erreur: %s" % result)
            return
        return json.loads(result.content.decode("utf-8"))

    @cool_request
    def get_activities(self, structure_id, filtres):
        # Recherche des activités filtrées d'une structure
        url = 'https://pegass.croix-rouge.fr/crf/rest/activite?structure=%s' % structure_id
        for filtre in filtres:
            url = '%s&%s=%s' % (url, filtre, filtres[filtre])
        result = self.session.get(url)
        if not result.ok:
            click.echo(" Erreur: %s" % result)
            return []
        return json.loads(result.content.decode("utf-8"))

    @cool_request
    def get_seance(self, seance_id):
        # Chargement d'unr séance spécifique
        url = 'https://pegass.croix-rouge.fr/crf/rest/seance/%s' % seance_id
        result = self.session.get(url)
        if not result.ok:
            click.echo(" Erreur: %s" % result)
            return
        return json.loads(result.content.decode("utf-8"))

    @cool_request
    def get_subscriptions(self, seance_id):
        # Recherche des inscriptions d'une séance
        url = 'https://pegass.croix-rouge.fr/crf/rest/seance/%s/inscription' % seance_id
        result = self.session.get(url)
        if not result.ok:
            click.echo(" Erreur: %s" % result)
            return []
        return json.loads(result.content.decode("utf-8"))

    @cool_request
    def get_user(self, user_id):
        # Chargement d'un utilisateur spécifique
        url = 'https://pegass.croix-rouge.fr/crf/rest/utilisateur/%s' % user_id
        result = self.session.get(url)
        if not result.ok:
            click.echo(" Erreur: %s" % result)
            return
        return json.loads(result.content.decode("utf-8"))


@click.group()
@click.pass_context
def update(context):
    "Cette commande permet de mettre à jour les données issues de PEGASS."
    # Création des répertoires nécessaires
    os.makedirs("datas/structures", exist_ok=True)
    os.makedirs("datas/activites", exist_ok=True)
    os.makedirs("datas/seances", exist_ok=True)
    os.makedirs("datas/inscriptions", exist_ok=True)
    os.makedirs("datas/utilisateurs", exist_ok=True)
    context.ensure_object(dict)


@update.command(help="Met à jour les structures.")
@click.pass_context
@with_appcontext
def structures(context):
    click.echo("Mise à jour des structures :")
    pegass = Pegass()
    if pegass.login() is True:
        structures = pegass.get_structures(config.DEPARTEMENT)
        for structure in structures["structuresFilles"]:
            structure_data = pegass.get_structure(structure["id"])
            click.echo(" Structure: %s (%s)" % (
                structure_data["libelle"],
                structure_data["id"],
            ))
            if structure_data is not None:
                with open(
                    "datas/structures/%s" % structure_data["id"],
                    "w",
                    encoding="utf-8"
                ) as f:
                    json.dump(structure_data, f)


@update.command(help="Met à jour les activités.")
@click.pass_context
@with_appcontext
def activities(context):
    click.echo("Mise à jour des activités :")
    pegass = Pegass()
    # On souhaite uniquement les n prochains jours et uniquement du secourisme
    filtres = {
        "debut": datetime.strftime(
            datetime.now(),
            "%Y-%m-%d"
        ),
        "fin": datetime.strftime(
            datetime.now()+timedelta(days=config.JOURS),
            "%Y-%m-%d"
        ),
        "groupeAction": 1,
    }
    # Chargement des activités pour chaque structure connue
    for structure_id in os.listdir("datas/structures/"):
        with open("datas/structures/%s" % structure_id) as f:
            structure = json.load(f)
        click.echo(" %s" % structure["libelle"])
        if pegass.login() is True:
            activites = pegass.get_activities(structure["id"], filtres)
            os.makedirs("datas/activites/%s" % structure_id, exist_ok=True)
            for activite in activites:
                click.echo("  Activite: %s (%s)" % (
                    activite["libelle"],
                    activite["id"],
                ))
                with open(
                    "datas/activites/%s/%s" % (structure_id, activite["id"]),
                    "w",
                    encoding="utf-8"
                ) as f:
                    json.dump(activite, f)


@update.command(help="Met à jour les séances.")
@click.pass_context
@with_appcontext
def sessions(context):
    click.echo("Mise à jour des séances :")
    pegass = Pegass()
    # Chargement des séances pour chaque activité connue
    for structure_id in os.listdir("datas/activites/"):
        with open("datas/structures/%s" % structure_id) as f:
            structure = json.load(f)
        click.echo(" %s" % structure["libelle"])
        os.makedirs("datas/seances/%s" % structure_id, exist_ok=True)
        for activite_id in os.listdir("datas/activites/%s" % structure_id):
            with open(
                "datas/activites/%s/%s" % (structure_id, activite_id)
            ) as f:
                activite = json.load(f)
            click.echo("  %s" % activite["libelle"])
            for seance in activite.get("seanceList", []):
                if pegass.login() is True:
                    seance_data = pegass.get_seance(seance["id"])
                    if seance_data is not None:
                        click.echo("   Séance: %s / %s" % (
                            seance_data["debut"],
                            seance_data["fin"],
                        ))
                        with open(
                            "datas/seances/%s/%s" % (structure_id, seance_data["id"]),
                            "w",
                            encoding="utf-8"
                        ) as f:
                            json.dump(seance_data, f)


@update.command(help="Met à jour les inscriptions.")
@click.pass_context
@with_appcontext
def subscriptions(context):
    click.echo("Mise à jour des inscriptions :")
    pegass = Pegass()
    # Chargement des inscriptions pour chaque séance connue
    for structure_id in os.listdir("datas/seances/"):
        with open("datas/structures/%s" % structure_id) as f:
            structure = json.load(f)
        click.echo(" %s" % structure["libelle"])
        os.makedirs("datas/inscriptions/%s" % structure_id, exist_ok=True)
        for seance_id in os.listdir("datas/seances/%s" % structure_id):
            with open(
                "datas/seances/%s/%s" % (structure_id, seance_id)
            ) as f:
                seance = json.load(f)
            click.echo("  %s" % seance["id"])
            if pegass.login() is True:
                inscriptions = pegass.get_subscriptions(seance["id"])
                with open(
                    "datas/inscriptions/%s/%s" % (structure_id, seance["id"]),
                    "w",
                    encoding="utf-8"
                ) as f:
                    json.dump(inscriptions, f)


@update.command(help="Met à jour les bénévoles.")
@click.pass_context
@with_appcontext
def users(context):
    click.echo("Mise à jour des bénévoles :")
    pegass = Pegass()
    utilisateurs = os.listdir("datas/utilisateurs/")
    # Chargement des utilisateurs pour chaque inscription
    for structure_id in os.listdir("datas/inscriptions/"):
        for seance_id in os.listdir("datas/inscriptions/%s" % structure_id):
            with open(
                "datas/inscriptions/%s/%s" % (structure_id, seance_id)
            ) as f:
                inscriptions = json.load(f)
            for inscription in inscriptions:
                # On ajoute uniquement les utilisateurs inconnus
                if inscription["utilisateur"]["id"] not in utilisateurs:
                    if pegass.login() is True:
                        utilisateur = pegass.get_user(inscription["utilisateur"]["id"])
                        utilisateurs.append(utilisateur["id"])
                        click.echo(" Utilisateur: %s %s (%s)" % (
                            utilisateur["prenom"],
                            utilisateur["nom"],
                            utilisateur["id"],
                        ))
                        with open(
                            "datas/utilisateurs/%s" % utilisateur["id"],
                            "w",
                            encoding="utf-8"
                        ) as f:
                            json.dump(utilisateur, f)


@update.command(help="Supprime les activités et séances trop anciennes.")
@click.pass_context
@with_appcontext
def clean(context):
    click.echo("Suppression des activités trop anciennes :")
    for structure_id in os.listdir(LIST_ACTIVITES):
        with open(PATH_STRUCTURE % structure_id) as f:
            structure = json.load(f)
        click.echo(" %s" % structure["libelle"])
        # Suppression des activités dépassées
        for activite_id in os.listdir(PATH_ACTIVITES % structure_id):
            with open(PATH_ACTIVITE % (structure_id, activite_id)) as f:
                activite = json.load(f)
            suppression_activite = True
            for seance in activite.get("seanceList", []):
                if datetime.strptime(
                    seance["fin"],
                    "%Y-%m-%dT%H:%M:%S"
                ) >= datetime.now()-timedelta(days=1):
                    suppression_activite = False
                    break
            if suppression_activite:
                click.echo("  %s" % activite["libelle"])
                os.remove(PATH_ACTIVITE % (structure_id, activite_id))
                for seance in activite.get("seanceList", []):
                    try:
                        os.remove(PATH_SEANCE % (structure_id, seance["id"]))
                    except:
                        pass
        # Suppression des séances sans activité
        for seance_id in os.listdir(PATH_SEANCES % structure_id):
            with open(PATH_SEANCE % (structure_id, seance_id)) as f:
                seance = json.load(f)
            if not os.path.isfile(PATH_ACTIVITE % (structure_id, seance['activite']['id'])):
                os.remove(PATH_SEANCE % (structure_id, seance_id))
        # Suppression des inscriptions sans activité
        for inscription_id in os.listdir(PATH_INSCRIPTIONS % structure_id):
            with open(PATH_INSCRIPTION % (structure_id, inscription_id)) as f:
                inscriptions = json.load(f)
            for inscription in inscriptions:
                if not os.path.isfile(PATH_ACTIVITE % (structure_id, inscription['activite']['id'])):
                    os.remove(PATH_INSCRIPTION % (structure_id, inscription_id))
                    break


@update.command(help="Supprime les activités et séances des loges trop anciennes.")
@click.pass_context
@with_appcontext
def clean_loges(context):
    LIST_SEANCES = os.path.join(config.DATA_PATH, "loges", "seances")
    PATH_SEANCES = os.path.join(LIST_SEANCES, "%s")
    LIST_ACTIVITES = os.path.join(config.DATA_PATH, "loges", "activites")
    PATH_ACTIVITES = os.path.join(LIST_ACTIVITES, "%s")
    LIST_INSCRIPTIONS = os.path.join(config.DATA_PATH, "loges", "inscriptions")
    PATH_INSCRIPTIONS = os.path.join(LIST_INSCRIPTIONS, "%s")
    click.echo("Suppression des activités des loges trop anciennes :")
    for activite_id in os.listdir(LIST_ACTIVITES):
        with open(PATH_ACTIVITES % activite_id) as f:
            activite = json.load(f)
        suppression_activite = True
        for seance in activite.get("seanceList", []):
            if datetime.strptime(
                seance["fin"],
                "%Y-%m-%dT%H:%M:%S"
            ) >= datetime.now()-timedelta(days=1):
                suppression_activite = False
                break
        if suppression_activite:
            click.echo("  %s" % activite["libelle"])
            os.remove(PATH_ACTIVITES % activite_id)
            for seance in activite.get("seanceList", []):
                try:
                    os.remove(PATH_SEANCES % seance["id"])
                except:
                    pass
        # Suppression des séances sans activité
        for seance_id in os.listdir(LIST_SEANCES):
            with open(PATH_SEANCES % seance_id) as f:
                seance = json.load(f)
            if not os.path.isfile(PATH_ACTIVITES % seance['activite']['id']):
                os.remove(PATH_SEANCES % seance_id)
        # Suppression des inscriptions sans activité
        for inscription_id in os.listdir(LIST_INSCRIPTIONS):
            with open(PATH_INSCRIPTIONS % inscription_id) as f:
                inscriptions = json.load(f)
            for inscription in inscriptions:
                if not os.path.isfile(PATH_ACTIVITES % inscription['activite']['id']):
                    os.remove(PATH_INSCRIPTIONS % inscription_id)
                    break


@update.command(help="Supprime les activités et séances des jop trop anciennes.")
@click.pass_context
@with_appcontext
def clean_jop(context):
    LIST_SEANCES = os.path.join(config.DATA_PATH, "jop", "seances")
    PATH_SEANCES = os.path.join(LIST_SEANCES, "%s")
    LIST_ACTIVITES = os.path.join(config.DATA_PATH, "jop", "activites")
    PATH_ACTIVITES = os.path.join(LIST_ACTIVITES, "%s")
    LIST_INSCRIPTIONS = os.path.join(config.DATA_PATH, "jop", "inscriptions")
    PATH_INSCRIPTIONS = os.path.join(LIST_INSCRIPTIONS, "%s")
    click.echo("Suppression des activités des jop trop anciennes :")
    for activite_id in os.listdir(LIST_ACTIVITES):
        with open(PATH_ACTIVITES % activite_id) as f:
            activite = json.load(f)
        suppression_activite = True
        for seance in activite.get("seanceList", []):
            if datetime.strptime(
                seance["fin"],
                "%Y-%m-%dT%H:%M:%S"
            ) >= datetime.now()-timedelta(days=1):
                suppression_activite = False
                break
        if suppression_activite:
            click.echo("  %s" % activite["libelle"])
            os.remove(PATH_ACTIVITES % activite_id)
            for seance in activite.get("seanceList", []):
                try:
                    os.remove(PATH_SEANCES % seance["id"])
                except:
                    pass
        # Suppression des séances sans activité
        for seance_id in os.listdir(LIST_SEANCES):
            with open(PATH_SEANCES % seance_id) as f:
                seance = json.load(f)
            if not os.path.isfile(PATH_ACTIVITES % seance['activite']['id']):
                os.remove(PATH_SEANCES % seance_id)
        # Suppression des inscriptions sans activité
        for inscription_id in os.listdir(LIST_INSCRIPTIONS):
            with open(PATH_INSCRIPTIONS % inscription_id) as f:
                inscriptions = json.load(f)
            for inscription in inscriptions:
                if not os.path.isfile(PATH_ACTIVITES % inscription['activite']['id']):
                    os.remove(PATH_INSCRIPTIONS % inscription_id)
                    break
