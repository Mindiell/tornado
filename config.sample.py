# encoding: utf-8

DEBUG = False
HOST = '0.0.0.0'
PORT = 5000
SECRET_KEY = 'Choose a secret key'

JINJA_ENV = {
    'TRIM_BLOCKS'   : True,
    'LSTRIP_BLOCKS' : True,
}

CREDENTIALS = {
    "username" : "username",
    "password" : "password",
}

DEPARTEMENT = "00"
DATA_PATH = "datas"
MAX_SEANCES = 10
BLACK_LIST = ()
JOURS = 90
