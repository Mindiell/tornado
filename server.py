# encoding: utf-8

from flask import Flask, session

app = Flask(__name__, template_folder='app/view')
app.config.from_object('config')
app.config.from_envvar('ENVIRONMENT_CONFIG', silent=True)
if 'JINJA_ENV' in app.config:
    app.jinja_env.trim_blocks = app.config['JINJA_ENV']['TRIM_BLOCKS']
    app.jinja_env.lstrip_blocks = app.config['JINJA_ENV']['LSTRIP_BLOCKS']

from app.routes import routes

# Loading routes
for route in routes:
    if len(route)<3:
        app.add_url_rule(route[0], route[1].__name__, route[1], methods=["GET"])
    else:
        app.add_url_rule(route[0], route[1].__name__, route[1], methods=route[2])

# Manage commands
from command import commands
for command in commands:
    app.cli.add_command(command)

if __name__=='__main__':
    app.run(
        debug = app.config['DEBUG'],
        host = app.config['HOST'],
        port = app.config['PORT'],
    )
