#encoding: utf-8

from datetime import datetime
import json
import os

from flask import g, render_template, request

import config
from app.controller.controller import Controller

STRUCTURES = [
    ("", "Secours 78"),
    ("DT78", "DT des yvelines"),
    ("7804", "UL de BSS"),
    ("7811", "UL des Mureaux"),
    ("7813", "UL de Mantes la jolie"),
    ("7815", "UL de St Quentin en yvelines"),
    ("7819", "UL de Poissy"),
    ("7820", "UL de Rambouillet"),
    ("7821", "UL de St Germain en laye"),
    ("7822", "UL de Sartrouville"),
    ("7825", "UL de Versailles"),
    ("7827", "UL de Viroflay"),
    ("DT92", "DT des Hauts de Seine"),
]

PEGASS_DAYS = ('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche')
PEGASS_MONTHES = ('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre',
    'Octobre', 'Novembre', 'Decembre')

PEGASS_STRUCTURES = {
    83: 'DT78',
    97: 'DT92',
    99: 'DT94',
    100: 'DT95',
    960: '7804',
    961: 'Chevreuse',
    967: '7811',
    969: '7813',
    970: 'Maule',
    971: '7815',
    974: '7819',
    975: '7820',
    976: '7821',
    977: '7822',
    980: '7825',
    982: '7827',
    4491: 'Pays Houdanais',
}

PEGASS_ROLES = {
    '167': 'CDMGE',
    'CDMGE': 'CDMGE',
    '288': 'CDPE',
    'CS': 'CS',
    '108': 'CS',
    'CP': 'CP',
    'CI': 'CI',
    '75': 'CI',
    '17': 'CI',
    '254': 'CI Réseau',
    'C': 'CH',
    '10': 'CH',
    '9': 'CH VL',
    '4': 'Cadre Territorial Operationnel',
    '358': 'Opérateur du Centre Opérationnel',
    '7': 'Cadre de permanence',
    '103': 'Responsable de PAPS',
    'ES': 'PSE2',
    '167': 'PSE2',
    'SEC': 'PSE1',
    '166': 'PSE1',
    'S': 'Stag.',
    'L': 'Log.',
    '47': 'OPR',
    '171': 'PSC1',
    'PARTICIPANT': 'Participant',
    'PARTICIPANT_EXT': 'Extérieur',
    'FORMATEUR': 'Formateur',
    '113': 'Initiateur aux premiers secours',
    'ASSISTANT': 'Assistant',
}

PEGASS_ORDRES_ROLES = {
    '4': 8,
    '7': 9,
    '358': 9,
    '167': 10,
    'CDMGE': 10,
    '288': 20,
    'CS': 25,
    '108': 25,
    'CP': 30,
    'CI': 40,
    '75': 40,
    '17': 40,
    '254': 40,
    'C': 50,
    '10': 50,
    '103': 52,
    '9': 55,
    'ES': 60,
    '167': 60,
    'SEC': 70,
    '166': 80,
    'S': 90,
    'L': 100,
    '47': 102,
    '171': 105,
    'PARTICIPANT': 110,
    'PARTICIPANT_EXT': 120,
    'FORMATEUR': 130,
    '113': 135,
    'ASSISTANT': 140,
}

PATH_STRUCTURES = os.path.join(config.DATA_PATH, "structures")
PATH_SEANCES = os.path.join(config.DATA_PATH, "seances", "%s")
PATH_SEANCE = os.path.join(PATH_SEANCES, "%s")
PATH_ACTIVITES = os.path.join(config.DATA_PATH, "activites", "%s")
PATH_ACTIVITE = os.path.join(PATH_ACTIVITES, "%s")
PATH_INSCRIPTIONS = os.path.join(config.DATA_PATH, "inscriptions", "%s")
PATH_INSCRIPTION = os.path.join(PATH_INSCRIPTIONS, "%s")
PATH_UTILISATEURS = os.path.join(config.DATA_PATH, "utilisateurs", "%s")

PATH_SEANCES_LOGES = os.path.join(config.DATA_PATH, "loges", "seances")
PATH_SEANCE_LOGES = os.path.join(PATH_SEANCES_LOGES, "%s")
PATH_ACTIVITES_LOGES = os.path.join(config.DATA_PATH, "loges", "activites")
PATH_ACTIVITE_LOGES = os.path.join(PATH_ACTIVITES_LOGES, "%s")
PATH_INSCRIPTIONS_LOGES = os.path.join(config.DATA_PATH, "loges", "inscriptions")
PATH_INSCRIPTION_LOGES = os.path.join(PATH_INSCRIPTIONS_LOGES, "%s")

PATH_SEANCES_JOP = os.path.join(config.DATA_PATH, "jop", "seances")
PATH_SEANCE_JOP = os.path.join(PATH_SEANCES_JOP, "%s")
PATH_ACTIVITES_JOP = os.path.join(config.DATA_PATH, "jop", "activites")
PATH_ACTIVITE_JOP = os.path.join(PATH_ACTIVITES_JOP, "%s")
PATH_INSCRIPTIONS_JOP = os.path.join(config.DATA_PATH, "jop", "inscriptions")
PATH_INSCRIPTION_JOP = os.path.join(PATH_INSCRIPTIONS_JOP, "%s")

PATH_SEANCES_RESEAU = os.path.join(config.DATA_PATH, "seances", "83")
PATH_SEANCE_RESEAU = os.path.join(PATH_SEANCES_RESEAU, "%s")
PATH_ACTIVITES_RESEAU = os.path.join(config.DATA_PATH, "activites", "83")
PATH_ACTIVITE_RESEAU = os.path.join(PATH_ACTIVITES_RESEAU, "%s")
PATH_INSCRIPTIONS_RESEAU = os.path.join(config.DATA_PATH, "inscriptions", "83")
PATH_INSCRIPTION_RESEAU = os.path.join(PATH_INSCRIPTIONS_RESEAU, "%s")


class Core(Controller):
    def home(self):
        return self.structure()

    def structure(self, structure=""):
        g.structures = STRUCTURES
        g.structure = structure
        g.url = "/"
        try:
            g.offset = max(0, int(request.args.get("o", 0)))
        except:
            g.offset = 0
        seances = []
        if g.structure == "":
            structures = os.listdir(PATH_STRUCTURES)
        else:
            structures = [s_id for s_id in PEGASS_STRUCTURES if PEGASS_STRUCTURES[s_id]==g.structure]
        for structure_id in structures:
            for seance_id in os.listdir(PATH_SEANCES % structure_id):
                with open(PATH_SEANCE % (structure_id, seance_id)) as f:
                    seance = json.loads(f.read())
                # Chargement des informations de l'activité elle-même
                with open(PATH_ACTIVITE % (structure_id, seance['activite']['id'])) as f:
                    activite = json.loads(f.read())
                # On ne s'occupe que des activités de type Poste de secours
                if activite["typeActivite"]["id"] not in (10113, 10109):
                    continue
                # Mise en forme des dates des la séance
                debut = datetime.strptime(seance['debut'], '%Y-%m-%dT%H:%M:%S')
                fin = datetime.strptime(seance['fin'], '%Y-%m-%dT%H:%M:%S')
                date = "%s %s %s %s de %02dh%02d à %02dh%02d" % (
                    PEGASS_DAYS[debut.weekday()],
                    debut.day,
                    PEGASS_MONTHES[debut.month-1],
                    debut.year,
                    debut.hour,
                    debut.minute,
                    fin.hour,
                    fin.minute,
                )
                libelle = activite['libelle']
                statut = activite['statut']
                # Si l'activité est finalement annulée (ou déjà close), on passe
                if activite['statut'] in ('Clos', 'Annulée', ):
                    continue
                # Si l'activité n'est pas un poste
                if len([word for word in config.BLACK_LIST if word in activite["libelle"]]) > 0:
                    continue
                # Chargement des informations de la structure liée
                try:
                    structure = PEGASS_STRUCTURES[activite['structureMenantActivite']['id']]
                except:
                    structure = "Externe"
                # Chargement des rôles nécessaires à la séance
                roles = {}
                for role in seance['roleConfigList']:
                    roles[role['code']] = {
                        'code': PEGASS_ROLES.get(role['code'], "Autre"),
                        'besoin': role['effectif'],
                        'manque': role['effectif'],
                        'ordre': PEGASS_ORDRES_ROLES.get(role['code'], 999),
                    }
                # Chargement des inscriptions de la séance
                utilisateurs = []
                try:
                    with open(PATH_INSCRIPTION % (structure_id, seance_id)) as f:
                        inscriptions = json.loads(f.read())
                except FileNotFoundError:
                    inscriptions = []
                for inscription in inscriptions:
                    role = inscription['role']
                    if role in roles:
                        roles[role]['manque'] = roles[role]['manque'] - 1
                    # Chargement des informations des utilisateurs inscrits à la séance
                    with open(PATH_UTILISATEURS % inscription['utilisateur']['id']) as f:
                        utilisateur = json.loads(f.read())
                    try:
                        u_structure = PEGASS_STRUCTURES[utilisateur['structure']['id']]
                    except:
                        u_structure = "Externe"
                    if role not in PEGASS_ROLES:
                        print(f"Rôle inconnu : {role}")
                    utilisateurs.append({
                        'id': utilisateur['id'],
                        'prenom': utilisateur['prenom'],
                        'nom': utilisateur['nom'],
                        'structure': u_structure,
                        'role': PEGASS_ROLES.get(role, "Autre"),
                        'ordre': PEGASS_ORDRES_ROLES.get(role, 999),
                        'statut': inscription["statut"],
                        'commentaire': inscription.get("commentaire", ""),
                        'debut': datetime.strftime(
                            datetime.strptime(inscription['debut'], '%Y-%m-%dT%H:%M:%S'),
                            '%Hh%M'
                        ),
                        'fin': datetime.strftime(
                            datetime.strptime(inscription['fin'], '%Y-%m-%dT%H:%M:%S'),
                            '%Hh%M'
                        ),
                    })
                # Gestion des manques
                manque = False
                for role in roles:
                    if roles[role]['manque']>0:
                        manque = True
                roles = sorted(roles.values(), key=lambda k: k['ordre'])
                utilisateurs = sorted(utilisateurs, key=lambda k: k['ordre'])
                manque_str = 'Manque %s' % ', '.join(
                    ['%s %s' % (
                        role['manque'],
                        role['code']
                    ) for role in roles if role['manque']>0]
                )
                # On ajoute la séance complète
                seances.append({
                    'id': seance_id,
                    'statut': statut,
                    'libelle': libelle,
                    'structure': structure,
                    'date': date,
                    'ordre': debut.timestamp(),
                    'manque': manque,
                    'manque_str': manque_str,
                    'roles': roles,
                    'utilisateurs': utilisateurs,
                })
        # On affiche un nombre limité de séances max en même temps
        seance_min = g.offset
        seance_max = g.offset + config.MAX_SEANCES
        if len(seances) <= g.offset:
            g.offset = len(seances) - 1
            seance_min = g.offset
            seance_max = g.offset + config.MAX_SEANCES
        g.seances = sorted(seances, key=lambda k: k['ordre'])[seance_min:seance_max]

        return render_template("home.html")

    def loges(self):
        g.url = "/"
        seances = []
        for seance_id in os.listdir(PATH_SEANCES_LOGES):
            with open(PATH_SEANCE_LOGES % seance_id) as f:
                seance = json.loads(f.read())
            # Chargement des informations de l'activité elle-même
            with open(PATH_ACTIVITE_LOGES % seance['activite']['id']) as f:
                activite = json.loads(f.read())
            # On ne s'occupe que des activités de type Poste de secours
            if activite["typeActivite"]["id"] not in (10113, 10109):
                continue
            # Mise en forme des dates des la séance
            debut = datetime.strptime(seance['debut'], '%Y-%m-%dT%H:%M:%S')
            fin = datetime.strptime(seance['fin'], '%Y-%m-%dT%H:%M:%S')
            date = "%s %s %s %s de %02dh%02d à %02dh%02d" % (
                PEGASS_DAYS[debut.weekday()],
                debut.day,
                PEGASS_MONTHES[debut.month-1],
                debut.year,
                debut.hour,
                debut.minute,
                fin.hour,
                fin.minute,
            )
            libelle = activite['libelle']
            statut = activite['statut']
            # Si l'activité est finalement annulée (ou déjà close), on passe
            if activite['statut'] in ('Clos', 'Annulée', ):
                continue
            # Chargement des informations de la structure liée
            try:
                structure = PEGASS_STRUCTURES[activite['structureMenantActivite']['id']]
            except:
                structure = "Externe"
            # Chargement des rôles nécessaires à la séance
            roles = {}
            try:
                for role in seance['roleConfigList']:
                    roles[role['code']] = {
                        'code': PEGASS_ROLES.get(role['code'], "Autre"),
                        'besoin': role['effectif'],
                        'manque': role['effectif'],
                        'ordre': PEGASS_ORDRES_ROLES.get(role['code'], 999),
                    }
            except KeyError:
                print(f"KeyError: {structure}, {date}, {seance_id}, {seance['activite']['id']}")
            # Chargement des inscriptions de la séance
            utilisateurs = []
            try:
                with open(PATH_INSCRIPTION_LOGES % seance_id) as f:
                    inscriptions = json.loads(f.read())
            except FileNotFoundError:
                inscriptions = []
            for inscription in inscriptions:
                role = inscription['role']
                if role in roles:
                    roles[role]['manque'] = roles[role]['manque'] - 1
                # Chargement des informations des utilisateurs inscrits à la séance
                with open(PATH_UTILISATEURS % inscription['utilisateur']['id']) as f:
                    utilisateur = json.loads(f.read())
                try:
                    u_structure = PEGASS_STRUCTURES[utilisateur['structure']['id']]
                except:
                    u_structure = "Externe"
                utilisateurs.append({
                    'id': utilisateur['id'],
                    'prenom': utilisateur['prenom'],
                    'nom': utilisateur['nom'],
                    'structure': u_structure,
                    'role': PEGASS_ROLES[role],
                    'ordre': PEGASS_ORDRES_ROLES[role],
                    'statut': inscription['statut'],
                    'commentaire': inscription.get('commentaire', ''),
                    'debut': datetime.strftime(
                        datetime.strptime(inscription['debut'], '%Y-%m-%dT%H:%M:%S'),
                        '%Hh%M'
                    ),
                    'fin': datetime.strftime(
                        datetime.strptime(inscription['fin'], '%Y-%m-%dT%H:%M:%S'),
                        '%Hh%M'
                    ),
                })
            # Gestion des manques
            manque = False
            for role in roles:
                if roles[role]['manque']>0:
                    manque = True
            roles = sorted(roles.values(), key=lambda k: k['ordre'])
            utilisateurs = sorted(utilisateurs, key=lambda k: k['ordre'])
            manque_str = 'Manque %s' % ', '.join(
                ['%s %s' % (
                    role['manque'],
                    role['code']
                ) for role in roles if role['manque']>0]
            )
            # On ajoute la séance complète
            seances.append({
                'id': seance_id,
                'statut': statut,
                'libelle': libelle,
                'structure': structure,
                'date': date,
                'ordre': debut.timestamp(),
                'manque': manque,
                'manque_str': manque_str,
                'roles': roles,
                'utilisateurs': utilisateurs,
            })
        g.seances = sorted(seances, key=lambda k: k['ordre'])

        return render_template("loges.html")

    def jop(self):
        g.url = "/"
        seances = []
        for seance_id in os.listdir(PATH_SEANCES_JOP):
            with open(PATH_SEANCE_JOP % seance_id) as f:
                seance = json.loads(f.read())
            # Chargement des informations de l'activité elle-même
            with open(PATH_ACTIVITE_JOP % seance['activite']['id']) as f:
                activite = json.loads(f.read())
            # On ne s'occupe que des activités de type Poste de secours
            if activite["typeActivite"]["id"] not in (10113, 10109):
                continue
            # Mise en forme des dates des la séance
            debut = datetime.strptime(seance['debut'], '%Y-%m-%dT%H:%M:%S')
            fin = datetime.strptime(seance['fin'], '%Y-%m-%dT%H:%M:%S')
            date = "%s %s %s %s de %02dh%02d à %02dh%02d" % (
                PEGASS_DAYS[debut.weekday()],
                debut.day,
                PEGASS_MONTHES[debut.month-1],
                debut.year,
                debut.hour,
                debut.minute,
                fin.hour,
                fin.minute,
            )
            libelle = activite['libelle']
            statut = activite['statut']
            # Si l'activité est finalement annulée (ou déjà close), on passe
            if activite['statut'] in ('Clos', 'Annulée', ):
                continue
            # Chargement des informations de la structure liée
            try:
                structure = PEGASS_STRUCTURES[activite['structureMenantActivite']['id']]
            except:
                structure = "Externe"
            # Chargement des rôles nécessaires à la séance
            roles = {}
            try:
                for role in seance['roleConfigList']:
                    roles[role['code']] = {
                        'code': PEGASS_ROLES.get(role['code'], "Autre"),
                        'besoin': role['effectif'],
                        'manque': role['effectif'],
                        'ordre': PEGASS_ORDRES_ROLES.get(role['code'], 999),
                    }
            except KeyError:
                print(f"KeyError: {structure}, {date}, {seance_id}, {seance['activite']['id']}")
            # Chargement des inscriptions de la séance
            utilisateurs = []
            try:
                with open(PATH_INSCRIPTION_JOP % seance_id) as f:
                    inscriptions = json.loads(f.read())
            except FileNotFoundError:
                inscriptions = []
            for inscription in inscriptions:
                role = inscription['role']
                if role in roles:
                    roles[role]['manque'] = roles[role]['manque'] - 1
                # Chargement des informations des utilisateurs inscrits à la séance
                with open(PATH_UTILISATEURS % inscription['utilisateur']['id']) as f:
                    utilisateur = json.loads(f.read())
                try:
                    u_structure = PEGASS_STRUCTURES[utilisateur['structure']['id']]
                except:
                    u_structure = "Externe"
                utilisateurs.append({
                    'id': utilisateur['id'],
                    'prenom': utilisateur['prenom'],
                    'nom': utilisateur['nom'],
                    'structure': u_structure,
                    'role': PEGASS_ROLES[role],
                    'ordre': PEGASS_ORDRES_ROLES[role],
                    'statut': inscription['statut'],
                    'commentaire': inscription.get('commentaire', ''),
                    'debut': datetime.strftime(
                        datetime.strptime(inscription['debut'], '%Y-%m-%dT%H:%M:%S'),
                        '%Hh%M'
                    ),
                    'fin': datetime.strftime(
                        datetime.strptime(inscription['fin'], '%Y-%m-%dT%H:%M:%S'),
                        '%Hh%M'
                    ),
                })
            # Gestion des manques
            manque = False
            for role in roles:
                if roles[role]['manque']>0:
                    manque = True
            roles = sorted(roles.values(), key=lambda k: k['ordre'])
            utilisateurs = sorted(utilisateurs, key=lambda k: k['ordre'])
            manque_str = 'Manque %s' % ', '.join(
                ['%s %s' % (
                    role['manque'],
                    role['code']
                ) for role in roles if role['manque']>0]
            )
            # On ajoute la séance complète
            seances.append({
                'id': seance_id,
                'statut': statut,
                'libelle': libelle,
                'structure': structure,
                'date': date,
                'ordre': debut.timestamp(),
                'manque': manque,
                'manque_str': manque_str,
                'roles': roles,
                'utilisateurs': utilisateurs,
            })
        g.seances = sorted(seances, key=lambda k: k['ordre'])

        return render_template("jop.html")

    def cad(self):
        g.url = "/"
        seances = []
        for seance_id in os.listdir(PATH_SEANCES_JOP):
            with open(PATH_SEANCE_JOP % seance_id) as f:
                seance = json.loads(f.read())
            # Chargement des informations de l'activité elle-même
            with open(PATH_ACTIVITE_JOP % seance['activite']['id']) as f:
                activite = json.loads(f.read())
            # On ne s'occupe que des activités de type Urgence
            if activite["typeActivite"]["id"] not in (10119, 10125):
                continue
            # Mise en forme des dates des la séance
            debut = datetime.strptime(seance['debut'], '%Y-%m-%dT%H:%M:%S')
            fin = datetime.strptime(seance['fin'], '%Y-%m-%dT%H:%M:%S')
            date = "%s %s %s %s de %02dh%02d à %02dh%02d" % (
                PEGASS_DAYS[debut.weekday()],
                debut.day,
                PEGASS_MONTHES[debut.month-1],
                debut.year,
                debut.hour,
                debut.minute,
                fin.hour,
                fin.minute,
            )
            libelle = activite['libelle']
            statut = activite['statut']
            # Si l'activité est finalement annulée (ou déjà close), on passe
            if activite['statut'] in ('Clos', 'Annulée', ):
                continue
            # Chargement des informations de la structure liée
            try:
                structure = PEGASS_STRUCTURES[activite['structureMenantActivite']['id']]
            except:
                structure = "Externe"
            # Chargement des rôles nécessaires à la séance
            roles = {}
            try:
                for role in seance['roleConfigList']:
                    roles[role['code']] = {
                        'code': PEGASS_ROLES.get(role['code'], "Autre"),
                        'besoin': role['effectif'],
                        'manque': role['effectif'],
                        'ordre': PEGASS_ORDRES_ROLES.get(role['code'], 999),
                    }
            except KeyError:
                print(f"KeyError: {structure}, {date}, {seance_id}, {activite['id']}")
            # Chargement des inscriptions de la séance
            utilisateurs = []
            try:
                with open(PATH_INSCRIPTION_JOP % seance_id) as f:
                    inscriptions = json.loads(f.read())
            except FileNotFoundError:
                inscriptions = []
            for inscription in inscriptions:
                role = inscription['role']
                if role in roles:
                    roles[role]['manque'] = roles[role]['manque'] - 1
                # Chargement des informations des utilisateurs inscrits à la séance
                with open(PATH_UTILISATEURS % inscription['utilisateur']['id']) as f:
                    utilisateur = json.loads(f.read())
                try:
                    u_structure = PEGASS_STRUCTURES[utilisateur['structure']['id']]
                except:
                    u_structure = "Externe"
                try:
                    utilisateurs.append({
                        'id': utilisateur['id'],
                        'prenom': utilisateur['prenom'],
                        'nom': utilisateur['nom'],
                        'structure': u_structure,
                        'role': PEGASS_ROLES[role],
                        'ordre': PEGASS_ORDRES_ROLES[role],
                        'statut': inscription['statut'],
                        'commentaire': inscription.get('commentaire', ''),
                        'debut': datetime.strftime(
                            datetime.strptime(inscription['debut'], '%Y-%m-%dT%H:%M:%S'),
                            '%Hh%M'
                        ),
                        'fin': datetime.strftime(
                            datetime.strptime(inscription['fin'], '%Y-%m-%dT%H:%M:%S'),
                            '%Hh%M'
                        ),
                    })
                except KeyError:
                    print(f"KeyError: {structure}, {date}, {seance_id}, {activite['id']}")
            # Gestion des manques
            manque = False
            for role in roles:
                if roles[role]['manque']>0:
                    manque = True
            roles = sorted(roles.values(), key=lambda k: k['ordre'])
            utilisateurs = sorted(utilisateurs, key=lambda k: k['ordre'])
            manque_str = 'Manque %s' % ', '.join(
                ['%s %s' % (
                    role['manque'],
                    role['code']
                ) for role in roles if role['manque']>0]
            )
            # On ajoute la séance complète
            seances.append({
                'id': seance_id,
                'statut': statut,
                'libelle': libelle,
                'structure': structure,
                'date': date,
                'ordre': debut.timestamp(),
                'manque': manque,
                'manque_str': manque_str,
                'roles': roles,
                'utilisateurs': utilisateurs,
            })
        g.seances = sorted(seances, key=lambda k: k['ordre'])

        return render_template("jop.html")

    def reseau(self):
        g.url = "/"
        seances = []
        for seance_id in os.listdir(PATH_SEANCES_RESEAU):
            with open(PATH_SEANCE_RESEAU % seance_id) as f:
                seance = json.loads(f.read())
            # Chargement des informations de l'activité elle-même
            with open(PATH_ACTIVITE_RESEAU % seance['activite']['id']) as f:
                activite = json.loads(f.read())
            # On ne s'occupe que des activités de type Urgence
            if activite["typeActivite"]["id"] not in (10116, 10115):
                continue
            # Mise en forme des dates des la séance
            debut = datetime.strptime(seance['debut'], '%Y-%m-%dT%H:%M:%S')
            fin = datetime.strptime(seance['fin'], '%Y-%m-%dT%H:%M:%S')
            date = "%s %s %s %s de %02dh%02d à %02dh%02d" % (
                PEGASS_DAYS[debut.weekday()],
                debut.day,
                PEGASS_MONTHES[debut.month-1],
                debut.year,
                debut.hour,
                debut.minute,
                fin.hour,
                fin.minute,
            )
            libelle = activite['libelle']
            statut = activite['statut']
            # Si l'activité est finalement annulée (ou déjà close), on passe
            if activite['statut'] in ('Clos', 'Annulée', ):
                continue
            # Chargement des informations de la structure liée
            try:
                structure = PEGASS_STRUCTURES[activite['structureMenantActivite']['id']]
            except:
                structure = "Externe"
            # Chargement des rôles nécessaires à la séance
            roles = {}
            try:
                for role in seance['roleConfigList']:
                    roles[role['code']] = {
                        'code': PEGASS_ROLES.get(role['code'], "Autre"),
                        'besoin': role['effectif'],
                        'manque': role['effectif'],
                        'ordre': PEGASS_ORDRES_ROLES.get(role['code'], 999),
                    }
            except KeyError:
                print(f"KeyError: {structure}, {date}, {seance_id}, {activite['id']}")
            # Chargement des inscriptions de la séance
            utilisateurs = []
            try:
                with open(PATH_INSCRIPTION_RESEAU % seance_id) as f:
                    inscriptions = json.loads(f.read())
            except FileNotFoundError:
                inscriptions = []
            for inscription in inscriptions:
                role = inscription['role']
                if role in roles:
                    roles[role]['manque'] = roles[role]['manque'] - 1
                # Chargement des informations des utilisateurs inscrits à la séance
                with open(PATH_UTILISATEURS % inscription['utilisateur']['id']) as f:
                    utilisateur = json.loads(f.read())
                try:
                    u_structure = PEGASS_STRUCTURES[utilisateur['structure']['id']]
                except:
                    u_structure = "Externe"
                try:
                    utilisateurs.append({
                        'id': utilisateur['id'],
                        'prenom': utilisateur['prenom'],
                        'nom': utilisateur['nom'],
                        'structure': u_structure,
                        'role': PEGASS_ROLES[role],
                        'ordre': PEGASS_ORDRES_ROLES[role],
                        'statut': inscription['statut'],
                        'commentaire': inscription.get('commentaire', ''),
                        'debut': datetime.strftime(
                            datetime.strptime(inscription['debut'], '%Y-%m-%dT%H:%M:%S'),
                            '%Hh%M'
                        ),
                        'fin': datetime.strftime(
                            datetime.strptime(inscription['fin'], '%Y-%m-%dT%H:%M:%S'),
                            '%Hh%M'
                        ),
                    })
                except KeyError:
                    print(f"KeyError: {structure}, {date}, {seance_id}, {activite['id']}")
            # Gestion des manques
            manque = False
            for role in roles:
                if roles[role]['manque']>0:
                    manque = True
            roles = sorted(roles.values(), key=lambda k: k['ordre'])
            utilisateurs = sorted(utilisateurs, key=lambda k: k['ordre'])
            manque_str = 'Manque %s' % ', '.join(
                ['%s %s' % (
                    role['manque'],
                    role['code']
                ) for role in roles if role['manque']>0]
            )
            # On ajoute la séance complète
            seances.append({
                'id': seance_id,
                'statut': statut,
                'libelle': libelle,
                'structure': structure,
                'date': date,
                'ordre': debut.timestamp(),
                'manque': manque,
                'manque_str': manque_str,
                'roles': roles,
                'utilisateurs': utilisateurs,
            })
        g.seances = sorted(seances, key=lambda k: k['ordre'])

        return render_template("reseau.html")

    def help(self):
        return render_template("help.html")
