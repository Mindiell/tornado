# encoding: utf-8

from app.controller.core import Core

routes = [
    ("/", Core.as_view("home")),
    ("/<structure>", Core.as_view("structure")),
    ("/loges", Core.as_view("loges")),
    ("/jop", Core.as_view("jop")),
    ("/cad", Core.as_view("cad")),
    ("/reseau", Core.as_view("reseau")),
    ("/help", Core.as_view("help")),
]
