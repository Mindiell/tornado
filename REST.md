# API REST de l'application PEGASS

L'url de base est : https://pegass.croix-rouge.fr/crf/rest/

Toute l'API est ensuite déclinée en requête spécifiques liées aux besoins.

## Constantes intéressantes

Le champ typeStructure prend les valeurs suivantes :
* AL : Antenne Locale
* DD : Direction Départementale
* DR : Direction Régionale
* EQ : Equipe Locale
* UL : Unité Locale

Le champ type pour les activités prend les valeurs suivantes :
* aa : 

Le champ categorie pour les activités prend les valeurs suivantes :
* AS : Action Sociale
* FORM : Formation
* VA : Vie Associative
* US : Urgence et Secourisme

Le champ type pour les rôles prend les valeurs suivantes :
* COMP : Compétence
* NOMI : Nomination
* FORM : Formation


## zonegeo

Ce service permet de lister les zones géographiques connues et utilisables.

URL : zonegeo/

Paramètres : Aucun

Retour : Une liste de zones géographiques. Liste de dictionnaires
* id : Identifiant de la zone géographique. Attention, il ne s'agit pas d'un entier (01!=1)
* nom : Nom de la zone géographique
* typeZoneGeo : Type de la zone géographique ("Département", "Région", "Instances Nationales")


### zonegeo::region

Ce service permet de lister les structures liées à une région.

URL : zonegeo/region/id

Paramètres :
* id : Identifiant de la région

Retour : Une liste de structures. Liste de dictionnaires
* id : Identifiant de la délégation
* libelle : Libelle de la délégation
* typeStructure : Type de la délégation ("DR", "DD", "EQ", "UL", "AL")


### zonegeo::departement

Ce service permet de lister les informations d'un département.

URL : zonegeo/departement/id

Paramètres :
* id : identifiant du département

Retour : Les informations du département. Dictionnaire
* id : Identifiant du departement
* nom : Nom du département
* typeZoneGeo : Type de la zone géographique ("Département")
* structuresFilles : Liste des structures du département. Liste de dictionnaires
    * id : Identifiant de la structure
    * libelle : Libelle de la structure
    * typeStructure : Type de la structure ("DR", "DD", "EQ", "UL", "AL")

## mazonegeoafficheepg

Ce service permet d'afficher les informations de la zone géographique affichée sur l'application
PEGASS. L'identifiant est lié à la session.

URL : mazonegeoafficheepg/

Paramètres : Aucun

Retour : Une zone géographique. Dictionnaire
* id : Identifiant de la zone géographique. Attention, il ne s'agit pas d'un entier (01!=1)
* nom : Nom de la zone géographique
* typeZoneGeo : Type de la zone géographique ("Département", "Région", "Instances Nationales")


## structure

Ce service permet de lister les structures connues et utilisables par paquet de 20.

URL : structure/

Paramètres : Aucun

Retour : Une liste de structures. Liste de dictionnaires
* id : Identifiant de la structure
* typeStructure : Type de la structure ("DR", "DD", "EQ", "UL", "AL")
* libelle : Libelle de la structure
* adresse (optionnel) : Adresse postale de la structure
* telephone (optionnel) : Numéro de téléphone de la structure. Attention, le numéro de téléphone ne contient que 9 chiffres
* mail (optionnel) : Adresse électronique de la structure
* parent (optionnel) : Dictionnaire
    * id : Identifiant de la structure parent
* structureMenantActiviteList (optionnel) : Liste des structures filles. Liste de dictionnaires
    * id : Identifiant de la structure fille
    * libelle : Libelle de la structure fille


### structure::id

Ce service permet d'afficher les informations d'une structure en particulier.

URL : structure/id

Paramètres :
* id : Identifiant de la structure

Retour : Une structure. Dictionnaire
* id : Identifiant de la structure
* typeStructure : Type de la structure ("DR", "DD", "EQ", "UL", "AL")
* libelle : Libelle de la structure
* adresse (optionnel) : Adresse postale de la structure
* telephone (optionnel) : Numéro de téléphone de la structure. Attention, le numéro de téléphone ne contient que 9 chiffres
* mail (optionnel) : Adresse électronique de la structure
* parent (optionnel) : Dictionnaire
    * id : Identifiant de la structure parent
* structureMenantActiviteList (optionnel) : Liste des structures filles. Liste de dictionnaires
    * id : Identifiant de la structure fille
    * libelle : Libelle de la structure fille


### structure::mastructureaffichee

Ce service permet d'afficher les informations de la structure affichée sur l'application PEGASS. Il
s'agit d'un alias vers le service structure/id avec l'identifiant lié à la session.

URL : structure/mastructureaffichee

Paramètres : Aucun

Retour : Une structure. Dictionnaire
* id : Identifiant de la structure
* typeStructure : Type de la structure ("DR", "DD", "EQ", "UL", "AL")
* libelle : Libelle de la structure
* adresse : Adresse postale de la structure
* telephone (optionnel) : Numéro de téléphone de la structure. Attention, le numéro de téléphone ne contient que 9 chiffres
* mail (optionnel) : Adresse électronique de la structure
* parent : Dictionnaire
    * id : Identifiant de la structure parent
* structureMenantActiviteList : Liste des structures filles. Liste de dictionnaires
    * id : Identifiant de la structure fille
    * libelle : Libelle de la structure fille

## seance::id

Ce service permet d'afficher les informations d'une séance.

URL : seance/id

Paramètres :
* id : Identifiant de la séance

Retour : L'activité. Dictionnaire
* id : Identifiant de la séance
* activite : Type de l'acitvité parent. Dictionnaire
    * id : Identifiant de l'activité parent
    * type : Type ("aa", )
* categorie : Catégorie de la séance ("FORM", "AS", "VA", "US")
* debut : Date de début de la séance
* fin : Date de fin de la séance
* adresse : 
* roleConfigList : Liste des rôles ouverts pour la séance. Liste de dictionnaires
    * id : Identifiant du rôle
    * code : Code du rôle
    * role : Nom du rôle
    * actif : Indique si le rôle est ouvert ou pas
    * effectif : Nombre de participants souhaités pour ce rôle
    * type : Type du rôle ("COMP")
* dimensionnementList (optionnel) : Liste des dimensionnements pour la séance. Liste de dictionnaires
    * libelle : Libellé du dimensionnement
    * nbPostes : Nombre de dimensionnements nécessaires


### seance::id::inscription

Ce service permet d'afficher les inscriptions à une séance spécifiée.

URL : seance/id/inscription

Paramètres :
* id : Identifiant de la séance

Query :
* utilisateur : Nivol d'un utilisateur. Cela permet de ne retourner que l'utilisateur spécifié s'il
est inscrit, sinon il s'agit d'une liste vide.
* debut : Date de début de la séance recherchée
* fin : Date de fin de la séance recherchée
* libelleLike : Libellé de la séance recherchée
* page : Page à afficher (0..n)
* pageInfo : (booléen)
* perPage : Nombre de séances à afficher (Défaut: 20)
* roleId : Code du rôle recherché
* roleType : Type du rôle recherché
* statut : Statut de la séance recherchée
* structure : Identifiant de la structure recherchée
* typeActivite : Type de l'activité parent de la séance recherchée
* responsable : Identifiant (NIVOL) du responsable de la séance
* categorie : Catégorie de la séance ("FORM", "AS", "VA", "US")

Retour : Les informations de la séance. Liste de dictionnaires
* id : Identifiant de l'inscription
* activite : Informations sur l'activité parent. Dictionnaire
    * id : Identifiant de l'activité parent
    * type : Type ("aa")
    * typeActivite : Type de l'activité. Dictionnaire
        * id : Identifiant du type de l'activité
        * libelle : Libellé du type de l'activité
    * statut : Statut de l'activité ("Complète", "Incomplète", "Annulée", "Clos")
    * categorie : Catégorie de l'activité ("FORM", "AS", "VA", "US")
* seance : L'activité spécifiée. Dictionnaire
    * id : Identifiant de la séance
* utilisateur : L'utilisateur inscrit à la séance. Dictionnaire
    * id : Identifiant (NIVOL) de l'utilisateur
    * actif : Indique si l'utilisateur est actif ou non (booléen)
* debut : Date de début de l'inscription à la séance
* fin : Date de fin de l'inscription à la séance
* statut : Statut de l'inscription ("VALIDEE", )
* role : Rôle de l'inscription ("PARTICIPANT", )
* type : Type de l'inscription ("COMP")
* isMultiple : (booléen)


## photo::id::size

Ce service permet de récupérer la photo.

URL : photo/

Paramètres :
* id : Identifiant (NIVOL) de l'utilisateur
* size : Taille de l'image à envoyer (xlarge, large, medium, small)

Retour : La photo au format jpg suivant la taille demandée
* xlarge : 120x120
* large : 100x100
* medium : 70x70
* small : 32x32


## activite

Ce service permet de lister les activités.

URL : activite/

Paramètres : Aucun

Query :
* debut : Date de début de l'activité recherchée
* fin : Date de fin de l'activité recherchée
* structure : Identifiant de la structure responsable de l'activité recherchée
* libelleLike : Libellé de l'activité recherchée
* page : Page à afficher (0..n, Défaut : 0)
* pageInfo : (booléen)
* perPage : Nombre d'activités à afficher (Défaut: 20)
* responsable : Identifiant (NIVOL) du responsable de l'activité recherchée
* categorie : Catégorie de la séance ("FORM", "AS", "VA", "US") (OBSOLETE)
* actionId : Action de l'activité cherchée
* groupeActionId : Groupe d'action de l'activité cherchée (OBSOLETE)
* typeActivite : Type de l'activité parent de l'activité recherchée
* groupeAction : Identifiant du groupe d'action de l'activité cherchée

Retour : Les activités filtrées. Dictionnaire
* list : La liste des activités trouvées. Liste de dictionnaires
    * id : Identifiant du type
    * type : Type ("af")
    * libelle : Libellé de l'activité
    * libelleCourt (optionnel) : Libellé court de l'activité
    * numSession (optionnel) : Numéro de session
    * typeActivite : Type de l'activité. Dictionnaire
        * id : Identifiant du type de l'activité
        * libelle : Libellé du type d'activité
        * groupeActionId : (entier)
        * actionId : (entier)
        * categorie : Catégorie de l'activité ("FORM", "AS", "VA", "US")
    * statut : Indique le statut de l'activité ("Complète", "Incomplète", "Annulée", "Clos")
    * structureOrganisatrice : Structure organisant l'acitivité. Dictionnaire
        * id : Identifiant de la structure organisant l'activité
    * structureMenantActivite : Structure menant l'acitivité. Dictionnaire
        * id : Identifiant de la structure menant l'activité
    * seanceList (optionnel) : Liste des séances. Liste de Dictionnaires
        * id : Identifiant de la séance
        * activite :
            * id : Identifiant de l'activité
            * type : Type de l'activité
        * categorie : Catégorie de la séance ("FORM", "AS", "VA", "US")
        * debut : Date de début de la séance
        * fin : Date de fin de la séance
        * adresse : Adresse de la séance
        * roleConfigList : Liste des rôles souhaités pour la séance. Liste de dictionnaires
            * code : Code du rôle. Attention, il ne s'agit pas d'un entier (01!=1)
            * role : Nom du rôle
            * actif : (booléen)
            * effectif : Effectif souhaité
            * idKeyQualification : (entier)
    * responsable : Responsable de l'activité. Dictionnaire
        * id : Identifiant (NIVOL) du responsable de l'activité
        * actif : (booléen)
    * categorie : Catégorie de l'activité ("FORM", "AS", "VA", "US")
* pages : Nombre total de pages
* page : Page actuelle
* perPage : Nombre d'activités à afficher (Défaut: 20)
* total : Nombre total de résultat


### activite::id

Ce service permet d'afficher les informations d'une activité spécifique.

URL : activite/id

Paramètres :
* id : Identifiant de l'activité

Retour : Les informations de l'activités spécifiée. Dictionnaire
* id : Identifiant de l'activité
* type : Type ("af", "aa")
* libelle : Libellé de l'activité
* libelleCourt (optionnel) : Libellé court de l'activité
* numSession (optionnel) : Numéro de session
* typeActivite : Type de l'activité. Dictionnaire
    * id : Identifiant du type de l'activité
    * libelle : Libellé du type d'activité
    * groupeActionId : Identifiant du groupe d'action du type d'activité
    * actionId : Identifiant de l'action du type d'activité
    * categorie (optionnel) : Catégorie de l'activité ("FORM", "AS", "VA", "US")
* statut : Indique le statut de l'activité ("Complète", "Incomplète", "Annulée", "Clos")
* structureOrganisatrice : Structure organisant l'acitivité. Dictionnaire
    * id : Identifiant de la structure organisant l'activité
* structureMenantActivite : Structure menant l'acitivité. Dictionnaire
    * id : Identifiant de la structure menant l'activité
* seanceList (optionnel) : Liste des séances. Liste de Dictionnaires
    * id : Identifiant de la séance
    * activite :
        * id : Identifiant de l'activité parent
        * type : Type de l'activité parent
    * categorie : Catégorie de la séance ("FORM", "AS", "VA", "US")
    * debut : Date de début de la séance
    * fin : Date de fin de la séance
    * adresse : Adresse de la séance
    * roleConfigList : Liste des rôles souhaités pour la séance. Liste de dictionnaires
        * code : Code du rôle. Attention, il ne s'agit pas d'un entier (01!=1)
        * role : Nom du rôle
        * actif : (booléen)
        * effectif : Effectif souhaité
        * idKeyQualification : (entier)
    * dimensionnementList (optionnel) : Liste des dimensionnements. Liste de dictionnaires
        * libelle : Libellé du dimensionnement
        * nbPostes : Nombre de dimensionnement
* responsable : Responsable de l'activité. Dictionnaire
    * id : Identifiant (NIVOL) du responsable de l'activité
    * actif : (booléen)
* categorie : Catégorie de l'activité ("FORM", "AS", "VA", "US")
* commentaire (optionnel) : Commentaire sur l'activité
* rappel (optionnel) : Indique si un rappel doit être envoyé aux isncrits


### activite::id::inscription

Ce service permet de lister les bénévoles inscrits à une activité spécifique.

URL : activite/id/inscription

Paramètres :
* id : Identifiant de l'activité

Retour : La liste des bénévoles inscrits à l'activités spécifiée. Liste de dictionnaires
* id : Identifiant de l'inscription
* activite :
    * id : Identifiant de l'activité parent
    * type : Type de l'activité parent
    * typeActivite (optionnel) : Type de l'activité parent. Dictionnaire
        * id : Identifiant du type de l'activité
        * libelle : Libellé du type d'activité
    * statut (optionnel) : Indique le statut de l'activité ("Complète", "Incomplète", "Annulée", "Clos")
    * categorie (optionnel) : Catégorie de l'activité ("FORM", "AS", "VA", "US")
* seance : Lien vers la séance. Dictionnaire
    * id : Identifiant de la séance
* utilisateur : Information de l'utilisateur inscrit. Dictionnaire
    * id : Identifiant (NIVOL) de l'utilisateur
    * actif : (booléen)
* debut : Date de début de l'inscription
* fin : Date de fin de l'inscription
* statut : Indique le statut de l'inscription ("VALIDEE", "EN ATTENTE")
* role : Code du rôle
* type (optionnel) : Type de l'inscription ("COMP")
* idKeyQualification : (entier)
* isMultiple : (booléen)


## roles

Ce service permet de lister les rôles disponibles dans l'application.

URL : roles/

Paramètres : Aucun

Retour : La liste des rôles disponibles. Liste de dictionnaires
* id : Identifiant/code du rôle. Attention, il ne s'agit pas d'un entier (01!=1)
* libelle : Nom du rôle
* type : Type du rôle ("COMP", "NOMI", "FORM")


## action

Ce service permet de lister les actions disponibles dans l'application.

URL : action/

Paramètres : Aucun

Retour : La liste des actions. Liste de dictionnaires
* id : Identifiant de l'action
* libelle : Nom de l'action


## categorie::groupeaction::action

Ce service permet de lister les actions d'un groupe d'action spécifique.

URL : action/

Paramètres : Aucun

Retour : La liste des actions. Liste de dictionnaires
* id : Identifiant de l'action
* libelle : Nom de l'action


## groupeaction

Ce service permet de lister les groupes d'action disponibles dans l'application.

URL : groupeaction/

Paramètres : Aucun

Retour : La liste des groupes d'action. Liste de dictionnaires
* id : Identifiant/code du groupe de l'action
* libelle : Nom du groupe d'action


### groupeaction::id

Ce service permet d'afficher le type d'action spécifié.

URL : groupeaction/id

Paramètres :
* id : Identifiant du type d'action

Retour : Les information du type d'action. Dictionnaire
* libelle : Nom du type d'action


## massmailingenabled

Ce service permet de connaitre les droits de l'utilisateur connecté quant à l'envoi massif de mails.

URL : massmailingenabled/

Paramètres : Aucun

Retour : Indique si l'utilisateur peut faire de l'envoi massif de mails. Dictionnaires
* massMailing : Indique si l'utilisateur peut faire de l'envoi massif de mails


## moyencomutilisateur

Ce service permet de lister les moyens de communication pour un utilisateur.

URL : moyencomutilisateur/

Paramètres : Aucun

Query :
* utilisateur : Identifiant (NIVOL) de l'utilisateur ciblé

Retour : Fournit les informations de communication pour l'utilisateur spécifié. Liste de dictionnaires
* id : Identifiant du moyen de communication
* utilisateurId : Identifiant (NIVOL) de l'utilisateur ciblé
* moyenComId : Identifiant du moyen de communication
* numero : Numéro de tri des moyens de communication
* libelle : Libellé du moyen de communication
* flag : Drapeau spécifique au moyen de communication (chaine)
* visible : Visibilité du moyen de communication
* canDelete : Indique si l'on peut supprimer le moyen de communication
* canUpdate : Indique si l'on peut modifier le moyen de communication


## secteur::departement

Ce service permet de lister les moyens de communication pour un utilisateur.

URL : secteur/departement

Paramètres : Aucun

Query :
* structure : Identifiant de la structure

Retour : Liste vide. Liste de dictionnaires


## utilisateur

Ce service permet de rechercher des utilisateurs.

URL : utilisateur/

Paramètres : Aucun

Query :
* pageInfo : booléen à true obligatoire
* page : Numéro de la page à afficher
* perPage : Nombre d'utilisateurs à afficher par page (Defaut: 20)
* zoneGeoId : Identifiant de la zone géographique recherchée
* zoneGeoType : Type de la zone géographique recherchée
* nomLike : Nom du bénévole recherché
* prenomLike : Prénom du bénévole recherché
* action : Action recherchée
* role : Code du rôle recherché
* structure : Identifiant de la structure recherchée
* like : Permet la recherche sur le Nivol, le prénom ou le nom d'un utilisateur

Retour : Liste des bénévoles trouvés, triée par nom croissant. Dictionnaire
* list : Liste des bénévoles trouvés. Liste de dictionnaires
    * id : Identifiant (NIVOL) du bénévole
    * structure : Structure d'appartenance du bénévole. Dictionnaire
        * id : Identifiant de la structure
    * nom : Nom du bénévole
    * prenom : Prénom du bénévole
    * actif : (booléen)
* page : Numéro de la page affichée (0..n)
* pages : Nombre total de pages à afficher
* total : Nombre total de bénévoles trouvés
* canAdmin : (booléen)
* perPage : Nombre de bénévoles listés par page


### utilisateur::id

Ce service permet de rechercher des utilisateurs.

URL : utilisateur/id

Paramètres :
* id : Identifiant (NIVOL) de l'utilisateur

Retour : Informations de l'utilisateur spécifié. Dictionnaire
* id : Identifiant (NIVOL) du bénévole
* structure : Structure d'appartenance du bénévole. Dictionnaire
    * id : Identifiant de la structure
* nom : Nom du bénévole
* prenom : Prénom du bénévole
* actif : (booléen)


## typeActivite

Ce service permet de lister les types d'activité.

URL : activite

Paramètres : Aucun

Query :
* perPage : Nombre de type d'activité par page
* page : Numéro de la page à afficher

Retour : Liste des types d'activité. Liste de dictionnaires
* id : Identifiant du type d'activité
* libelle : Libellé du type d'activité
* categorie : Catégorie du type d'activité
* groupeActionId : Identifiant du groupe d'action lié
* actionId : Identifiant de l'action liée
* actif : (booléen)


## competences

Ce service permet de lister les compétences.

URL : competences/

Paramètres : Aucun

Retour : Liste des compétences. Liste de dictionnaires
* id : Identifiant de la compétence
* libelle : Libellé de la compétence
* actif : (booléen)
* readOnly : (booléen)


### competences::id

Ce service permet d'afficher les informations d'une compétence.

URL : competences/id

Paramètres :
* id : Identifiant de la compétence à afficher

Retour : Informations d'une compétence. Dictionnaire
* id : Identifiant de la compétence
* libelle : Libellé de la compétence
* actif : (booléen)
* readOnly : (booléen)


## moyencom

Ce service permet de lister les moyens de communication.

URL : moyencom/

Paramètres : Aucun

Retour : Liste des moyens de communication. Liste de dictionnaires
* id : Identifiant du moyen de communication
* categorieMoyenCom : Identifiant de la catégorie du moyen de communication
* libelle : Libellé du moyen de communication


### moyencom::id

Ce service permet d'afficher les informations d'un moyen de communication.

URL : moyencom/id

Paramètres :
* id : Identifiant du moyen de communication à afficher

Retour : Informations du moyen de communication. Dictionnaire
* id : Identifiant du moyen de communication
* categorieMoyenCom : Identifiant de la catégorie du moyen de communication
* libelle : Libellé du moyen de communication


## categoriemoyencom

Ce service permet de lister les catégories de moyen de communication.

URL : categoriemoyencom/

Paramètres : Aucun

Retour : Liste des catégories de moyens de communication. Liste de dictionnaires
* id : Identifiant de la catégorie du moyen de communication
* libelle : Libellé de la catégorie du moyen de communication


### categoriemoyencom::id

Ce service permet d'afficher les informations d'une catégorie de moyen de communication.

URL : categoriemoyencom/id

Paramètres :
* id : Identifiant de la catégorie du moyen de communication à afficher

Retour : Informations de la catégorie du moyen de communication. Dictionnaire
* id : Identifiant de la catégorie du moyen de communication
* libelle : Libellé de la catégorie du moyen de communication


## gestiondesdroits

Ce service liste les droits de l'utilisateur connecté.

URL : gestiondesdroits

Paramètres : Aucun

Retour : Informations sur les droits d'administration de l'utilisateur connecté. Dictionnaire
* utilisateur : Utilisateur connecté. Dictionnaire
    * id : Identifiant (NIVOL) de l'utilisateur connecté
    * structure : Structure d'appartenance de l'utilisateur connecté. Dictionnaire
        * id : Identifiant de la structure
    * nom : Nom de l'utilisateur connecté
    * prénom : Prénom de l'utilisateur connecté
    * actif : (booléen)
* structuresAdministrees : Liste des structures administrées par l'utilisateur connecté. Liste de dictionnaires
    * id : Identifiant de la structure
    * typeStructure : Type de la structure
    * libelle : Libellé de la structure


### gestiondesdroits::id

Ce service liste les droits de l'utilisateur spécifié.

URL : gestiondesdroits.id

Paramètres :
* id : Identifiant (NIVOL) de l'utilisateur

Retour : Informations sur les droits d'administration de l'utilisateur spécifié. Dictionnaire
* utilisateur : Utilisateur connecté. Dictionnaire
    * id : Identifiant (NIVOL) de l'utilisateur spécifié
    * structure : Structure d'appartenance de l'utilisateur spécifié. Dictionnaire
        * id : Identifiant de la structure
    * nom : Nom de l'utilisateur spécifié
    * prénom : Prénom de l'utilisateur spécifié
    * actif : (booléen)
* structuresAdministrees : Liste des structures administrées par l'utilisateur spécifié. Liste de dictionnaires
    * id : Identifiant de la structure
    * typeStructure : Type de la structure
    * libelle : Libellé de la structure


### gestiondesdroits::peutadministrerutilisateur

Ce service indique si l'utilisateur connecté peut administrer un utilisateur spécifié.

URL : gestiondesdroits/peutadministrerutilisateur

Paramètres : Aucun

Query :
* utilisateur : Identifiant (NIVOL) de l'utilisateur

Retour : Booléen indiquant si l'utilisateur spécifié peut être administré par l'utilisateur connecté. Dictionnaire
* peutAdministrer : (booléen)


## structureaction

Ce service indique les actions qu'un utilisateur peut effectuer et pour quelles structures.

URL : structureaction/

Paramètres : Aucun

Query :
* utilisateur : Identifiant (NIVOL) de l'utilisateur

Retour : Liste d'actions. Liste de dictionnaires
* id : Identifiant de l'action faisable par l'utilisateur
* structure : Structure au sein de laquelle l'utilisateur est habilité à faire l'action
    * id : Identifiant de la structure
    * libelle : Libellé de la structure
* groupeAction : 
    * id : Identifiant du groupe d'action
    * libelle : Libellé du groupe d'action
* action : 
    * id : Identifiant de l'action
    * libelle : Libellé de l'action
* dateEntree : Date de début d'habilitation


## infoutilisateur::id

Ce service liste les informations d'un utilisateur.

URL : infoutilisateur/id

Paramètres :
* id : Identifiant (NIVOl) de l'utilisateur

Retour : Liste des informations de l'utilisateur. Dictionnaire
* id : Identifiant (NIVOl) de l'utilisateur
* inscriptionsExternes : (booléen)
* contactParMail : (booléen)
* listeRouge : (booléen)
* mailMoyenComId : Identifiant du mail de contact à utiliser
* dateNaissance : Date de naissance de l'utilisateur
* sexe : Sexe de l'utilisateur ("H", "F")


## structurepreferee::id

Ce service renvoit la structure préférée (si définie) d'un utilisateur.

URL : structurepreferee/id

Paramètres :
* id : Identifiant (NIVOl) de l'utilisateur

Retour : Structure préférée de l'utilisateur. Dictionnaire

Si Pas de structure, une chaine est renvoyée : "StructurePrefereeQueryController : L'utilisateur (id=XXXX) n'a pas de structure préférée" 


## competenceutilisateur::id

Ce service renvoit la liste des compétences d'un utilisateur.

URL : competenceutilisateur/id

Paramètres :
* id : Identifiant (NIVOl) de l'utilisateur

Retour : Liste des compétences de l'utilisateur. Liste de dictionnaires
* id : Identifiant de la compétence
* libelle : Libellé de la compétence
* dateValidation (optionnel) : Date de validation de la compétence


## nominationutilisateur

Ce service renvoit la liste des compétences d'un utilisateur.

URL : nominationutilisateur

Paramètres : Aucun

Query :
* id : Identifiant (NIVOl) de l'utilisateur

Retour : Liste des nominations de l'utilisateur. Liste de dictionnaires
* id : Identifiant de la nomination
* groupeNomination : groupe de la nomination. Dictionnaire
    * id : Identifiant du groupe de la nomination
    * libelle : libelle du groupe de la nomination
* structure : structure de la nomination. Dictionnaire
    * id : Identifiant de la structure
    * libelle : libelle de la structure
* libelleLong : Libellé long de la nomination
* libelleCourt : Libellé court de la nomination
* dateValidation (optionnel) : Date de validation de la nomination


## formationutilisateur::id

Ce service renvoit la liste des formations d'un utilisateur.

URL : formationutilisateur

Paramètres : Aucun

Query :
* id : Identifiant (NIVOl) de l'utilisateur

Retour : Liste des formations de l'utilisateur. Liste de dictionnaires
* id : Identifiant de la formation
* formation : Formation. Dictionnaire
    * id : Identifiant de la formation
    * code : Code de la formation
    * libelle : Libellé de la formation
    * recyclage : Indique si un recyclage est nécessaire pour cette formation 
* dateObention : Date d'obtention de la formation
* dateRecyclage : Date limite de recyclage pour la formation



## URLs non examinées

...


## Utilisations

Requête permettant de lister les activités de l'UL UUUUU du 1er au 2 octobre 2017

    https://pegass.croix-rouge.fr/crf/rest/activite?structure=UUUUU&debut=2017-10-01&fin=2017-10-02

J'obtiens alors une seule activité avec un id _XXXXXX_

Requête permettant d'afficher les informations spécifiques à une activité

    https://pegass.croix-rouge.fr/crf/rest/activite/XXXXXX

Requête permettant d'afficher les inscriptions à une activité

    https://pegass.croix-rouge.fr/crf/rest/activite/XXXXXX/inscription

Requête permettant d'afficher les inscriptions à une séance en particulier

    https://pegass.croix-rouge.fr/crf/rest/séance/YYYYYY/inscription

Requête permettant d'afficher les activités de type "Urgence et Secourisme" à venir d'une DT

    https://pegass.croix-rouge.fr/crf/rest/activite?structure=XXX&debut=TODAY&categorie=US





